use std::process::Command;

#[test]
#[cfg(not(tarpaulin))]
fn test_bin_callpass_single() {
    let callsign: &str = "abc123";
    let callpass: u16 = 17314;

    let output = Command::new("callpass")
        .args([callsign])
        .output()
        .expect("Not able to test command output!");
    assert!(
        output.status.success(),
        "callpass tool did not exit cleanly!"
    );

    let stdout = String::from_utf8_lossy(&output.stdout);
    assert_eq!(
        stdout,
        format!("{callpass}\n"),
        "Callpass was not correctly generated!"
    );
}

#[test]
#[cfg(not(tarpaulin))]
fn test_bin_callpass_multiple() {
    let callpasses: Vec<(&str, u16)> = vec![
        ("abc123", 17314),
        ("456def", 13525),
        ("hijklm", 15789),
        ("789012", 19672),
    ];

    let output = Command::new("callpass")
        .args(callpasses.iter().map(|(k, _v)| *k).collect::<Vec<&str>>())
        .output()
        .expect("Not able to test command output!");
    assert!(
        output.status.success(),
        "callpass tool did not exit cleanly!"
    );

    let stdout = String::from_utf8_lossy(&output.stdout);
    let expected_stdout = callpasses
        .iter()
        .map(|(_k, v)| *v)
        .fold(String::new(), |acc, val| acc + &val.to_string() + "\n");
    assert_eq!(
        stdout, expected_stdout,
        "Callpass was not correctly generated!"
    );
}
