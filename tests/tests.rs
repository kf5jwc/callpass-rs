extern crate callpass;

use callpass::Callpass;

#[test]
fn correctness() {
    assert!(Callpass::from("ab2def") == 17826);
    assert!(Callpass::from("xyzmb2") == 13252);
}

#[test]
fn comparison() {
    let callpass = Callpass::from("12345");
    assert!(callpass == 17636u16);
    assert!(callpass != 00000u16);
}

#[cfg(feature = "std")]
#[test]
fn into_string() {
    let callpass = Callpass::from("12345");
    let test: String = callpass.into();
    assert_eq!(test, "17636".to_string());
}

#[cfg(feature = "std")]
#[test]
fn zeros_padding() {
    assert_eq!(format!("{}", Callpass::from("a--")), "08143");
    assert_eq!(format!("{}", Callpass::from("a+%!")), "06120");
}

#[cfg(feature = "std")]
#[test]
fn from_string() {
    let callpass = Callpass::from("12345".to_string());
    let callsign = "12345".to_string();
    assert_eq!(callpass, Callpass::from(callsign));
}

#[test]
#[should_panic]
fn from_string_non_ascii() {
    let non_ascii_string = "🧰".to_string();
    let _: Callpass = non_ascii_string.into();
}

#[cfg(feature = "std")]
#[test]
fn from_string_borrow() {
    let callpass = Callpass::from(&"12345".to_string());
    let callsign = "12345".to_string();
    assert_eq!(callpass, Callpass::from(&callsign));
}

#[test]
#[should_panic]
fn from_string_non_ascii_borrow() {
    let non_ascii_string = "🧰".to_string();
    let _: Callpass = (&non_ascii_string).into();
}

#[test]
fn from_str() {
    let callpass = Callpass::from("12345");
    let callsign: &str = "12345";
    assert_eq!(callpass, Callpass::from(callsign));
}

#[test]
#[should_panic]
fn from_str_non_ascii() {
    let non_ascii_str = "🧰";
    let _: Callpass = non_ascii_str.into();
}

#[test]
fn into_u64() {
    let callpass = Callpass::from("12345");
    let test: u64 = callpass.into();
    assert_eq!(test, 17636u64);
}

#[test]
fn from_u64() {
    let callpass = Callpass::from("12345");
    let test: Callpass = 17636u64.into();
    assert_eq!(test, callpass);
}

#[test]
fn from_u64_borrow() {
    let callpass = Callpass::from("12345");
    let test: Callpass = (&17636u64).into();
    assert_eq!(test, callpass);
}

#[test]
fn into_i64() {
    let callpass = Callpass::from("12345");
    let test: i64 = callpass.into();
    assert_eq!(test, 17636i64);
}

#[test]
fn from_i64() {
    let callpass = Callpass::from("12345");
    let test: Callpass = 17636i64.into();
    assert_eq!(test, callpass);
}

#[test]
fn from_i64_borrow() {
    let callpass = Callpass::from("12345");
    let test: Callpass = (&17636i64).into();
    assert_eq!(test, callpass);
}

#[test]
fn into_u32() {
    let callpass = Callpass::from("12345");
    let test: u32 = callpass.into();
    assert_eq!(test, 17636u32);
}

#[test]
fn from_u32() {
    let callpass = Callpass::from("12345");
    let test: Callpass = 17636u32.into();
    assert_eq!(test, callpass);
}

#[test]
fn from_u32_borrow() {
    let callpass = Callpass::from("12345");
    let test: Callpass = (&17636u32).into();
    assert_eq!(test, callpass);
}

#[test]
fn into_i32() {
    let callpass = Callpass::from("12345");
    let test: i32 = callpass.into();
    assert_eq!(test, 17636i32);
}

#[test]
fn from_i32() {
    let callpass = Callpass::from("12345");
    let test: Callpass = 17636i32.into();
    assert_eq!(test, callpass);
}

#[test]
fn from_i32_borrow() {
    let callpass = Callpass::from("12345");
    let test: Callpass = (&17636i32).into();
    assert_eq!(test, callpass);
}

#[test]
fn into_u16() {
    let callpass = Callpass::from("12345");
    let test: u16 = callpass.into();
    assert_eq!(test, 17636u16);
}

#[test]
fn from_u16() {
    let callpass = Callpass::from("12345");
    let test: Callpass = 17636u16.into();
    assert_eq!(test, callpass);
}

#[test]
fn from_u16_borrow() {
    let callpass = Callpass::from("12345");
    let test: Callpass = (&17636u16).into();
    assert_eq!(test, callpass);
}

#[test]
fn into_i16() {
    let callpass = Callpass::from("12345");
    let test: i16 = callpass.into();
    assert_eq!(test, 17636i16);
}

#[test]
fn from_i16() {
    let callpass = Callpass::from("12345");
    let test: Callpass = 17636i16.into();
    assert_eq!(test, callpass);
}

#[test]
fn from_i16_borrow() {
    let callpass = Callpass::from("12345");
    let test: Callpass = (&17636i16).into();
    assert_eq!(test, callpass);
}
