macro_rules! generate_impl_from_numeric {
    ($($x:ty),*) => {
        $(impl From<$x> for Callpass {
            fn from(input: $x) -> Callpass {
                Callpass { 0: input as u16 }
            }
        })*

        $(impl<'a> From<&'a $x> for Callpass {
            fn from(input: &'a $x) -> Callpass {
                Callpass { 0: *input as u16 }
            }
        })*

        $(impl From<Callpass> for $x {
            fn from(callpass: Callpass) -> $x {
                callpass.0 as $x
            }
        })*
    }
}

macro_rules! generate_impl_partialeq {
    ($($x:ty),*) => {
        $(impl PartialEq<$x> for Callpass {
            fn eq(&self, other: &$x) -> bool {
                ((self.0 as $x) == *other)
            }
        })*
    }
}
