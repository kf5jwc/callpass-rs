extern crate callpass;
extern crate docopt;
extern crate serde_derive;

use callpass::Callpass;
use docopt::Docopt;
use serde_derive::Deserialize;

const USAGE: &str = "
Usage:
    callpass [--verbose] <callsign>...

Options:
    --verbose, -v   Include the callsign after the callpass
";

#[derive(Deserialize)]
struct Args {
    arg_callsign: Vec<String>,
    flag_verbose: bool,
}

pub fn main() {
    let args: Args = get_args(USAGE);

    for callsign in args.arg_callsign {
        let callpass: Callpass = (&callsign).into();
        match args.flag_verbose {
            true => println!("{} {}", callpass, callsign),
            false => println!("{}", callpass),
        };
    }
}

fn get_args(usage: &str) -> Args {
    Docopt::new(usage)
        .and_then(|d| d.deserialize())
        .unwrap_or_else(|e| e.exit())
}
